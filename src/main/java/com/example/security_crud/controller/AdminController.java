package com.example.security_crud.controller;

import com.example.security_crud.dto.UserDto;
import com.example.security_crud.mapper.UserMapper;
import com.example.security_crud.model.User;
import com.example.security_crud.service.role.RoleService;
import com.example.security_crud.service.user.UserService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@Controller
@RequestMapping("/admin")
public class AdminController {

    private final UserService userService;
    private final UserMapper userMapper;
    private final RoleService roleService;
    private final PasswordEncoder passwordEncoder;

    public AdminController(UserService userService,
                           UserMapper userMapper,
                           RoleService roleService,
                           @Qualifier("bcryptPasswordEncoder") PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.roleService = roleService;
        this.passwordEncoder = passwordEncoder;
    }

    @GetMapping
    public String adminPage(Principal principal, Model model) {
        model.addAttribute("users", userService.getAllUsers());
        model.addAttribute("newUser", new User());
        model.addAttribute("currentUser", userService.getByEmail(principal.getName()));
        model.addAttribute("roles", roleService.getAllRoles());
        return "admin";
    }

    @PostMapping("/create")
    public String createUser(@ModelAttribute("user") UserDto userHolder) {
        User user = userMapper.dtoToUser(userHolder);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userService.addUser(user);
        return "redirect:/admin";
    }

    @PostMapping("/delete/{id}")
    public String deleteUser(@PathVariable Long id) {
        userService.deleteUser(id);
        return "redirect:/admin";
    }

    @PostMapping("/update/{id}")
    public String updateUser(@ModelAttribute("user") UserDto userHolder,
                             @PathVariable Long id) {
        User user = userMapper.dtoToUser(userHolder);

        String oldPassword = userService.getPasswordByUserId(user.getId());
        if (!oldPassword.equals(user.getPassword())) {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
        }
        user.setId(id);

        userService.updateUser(user);
        return "redirect:/admin";
    }
}
