package com.example.security_crud.controller;

import com.example.security_crud.model.User;
import com.example.security_crud.service.user.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.security.Principal;

@Controller
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/user")
    public String userPage(Principal principal, Model model) {
        User user = userService.getByEmail(principal.getName());
        model.addAttribute("currentUser", user);
        return "userPage";
    }

    @GetMapping("/")
    public String redirectToUserPage() {
        return "redirect:/user";
    }

}
