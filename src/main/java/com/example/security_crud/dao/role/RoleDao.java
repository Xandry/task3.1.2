package com.example.security_crud.dao.role;


import com.example.security_crud.model.Role;
import com.example.security_crud.model.User;

import java.util.List;

public interface RoleDao {
    List<Role> getAllRoles();

    List<Long> getAllRolesIds();

    void update(Role role);

    void delete(long id);

    void add(Role role);

    void addAllRolesOfUser(User user);

    void removeAllRolesOfUser(long id);

    void removeRolesOfUser(long userId, List<Long> rolesId);

    void addRolesToUser(long userId, List<Long> rolesIds);

    List<Long> getAllRolesIdsOfUser(long userId);

    List<Role> getRolesByIds(List<Long> ids);
}
