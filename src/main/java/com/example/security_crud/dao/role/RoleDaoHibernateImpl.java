package com.example.security_crud.dao.role;

import com.example.security_crud.model.Role;
import com.example.security_crud.model.User;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class RoleDaoHibernateImpl implements RoleDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Role> getAllRoles() {
        TypedQuery<Role> query = entityManager.createQuery("from Role", Role.class);
        return query.getResultList();
    }

    @Override
    public List<Long> getAllRolesIds() {
        TypedQuery<Long> query = entityManager.createQuery("select r.id from Role r", Long.class);
        return query.getResultList();
    }

    @Override
    public void update(Role role) {
        entityManager.merge(role);
    }

    @Override
    public void delete(long id) {
        Query query = entityManager.createQuery("delete from Role where id = :id");
        query.setParameter("id", id);
        query.executeUpdate();
    }

    @Override
    public void add(Role role) {
        entityManager.persist(role);
    }

    @Override
    public void addAllRolesOfUser(User user) {
        addRolesToUser(user.getId(), user.getRoles().stream().map(Role::getId).collect(Collectors.toList()));
    }


    @Override
    public void removeAllRolesOfUser(long id) {
        Query nativeQuery = entityManager.createNativeQuery("delete from user_has_role where user_id = ?");
        nativeQuery.setParameter(1, id);
        nativeQuery.executeUpdate();
    }

    @Override
    public void removeRolesOfUser(long userId, List<Long> rolesIds) {
        if (!rolesIds.isEmpty()) {
            String sqlConstructor = "delete from user_has_role where user_id = ? and role_id in ("
                    + "?,".repeat(rolesIds.size() - 1)
                    + "?);";
            Query nativeQuery = entityManager.createNativeQuery(sqlConstructor);
            nativeQuery.setParameter(1, userId);
            int parameterNumber = 2;
            for (Long roleId : rolesIds) {
                nativeQuery.setParameter(parameterNumber++, roleId);
            }
            nativeQuery.executeUpdate();
        }
    }

    @Override
    public void addRolesToUser(long userId, List<Long> rolesIds) {
        if (!rolesIds.isEmpty()) {
            String sqlConstructor = "insert user_has_role (user_id, role_id) values "
                    + "(?, ?), ".repeat(rolesIds.size() - 1)
                    + "(?, ?)";
            Query nativeQuery = entityManager.createNativeQuery(sqlConstructor);
            int parameterNumber = 1;
            for (Long roleId : rolesIds) {
                nativeQuery.setParameter(parameterNumber++, userId);
                nativeQuery.setParameter(parameterNumber++, roleId);
            }
            nativeQuery.executeUpdate();
        }
    }


    @Override
    @SuppressWarnings("unchecked")
    public List<Long> getAllRolesIdsOfUser(long userId) {
        Query nativeQuery = entityManager.createNativeQuery("select role_id from user_has_role where user_id = ?");
        nativeQuery.setParameter(1, userId);
        List<Number> resultList = nativeQuery.getResultList();
        return resultList.stream().map(Number::longValue).collect(Collectors.toList());

    }

    @Override
    public List<Role> getRolesByIds(List<Long> ids) {
        if (!ids.isEmpty()) {
            TypedQuery<Role> query = entityManager.createQuery("from Role where id in (:ids)", Role.class);
            query.setParameter("ids", ids);
            return query.getResultList();
        } else {
            return Collections.emptyList();
        }
    }
}
