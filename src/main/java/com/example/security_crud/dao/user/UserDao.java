package com.example.security_crud.dao.user;

import com.example.security_crud.model.User;

import java.util.List;

public interface UserDao {

    User getUser(long id);

    String getUserPassword(long id);

    User getUserByEmail(String email);

    List<User> getAllUsers();

    void deleteUser(long id);

    void deleteAll();

    void addUser(User user);

    void updateUser(User user);

}
