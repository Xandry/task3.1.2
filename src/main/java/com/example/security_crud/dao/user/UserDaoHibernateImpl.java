package com.example.security_crud.dao.user;

import com.example.security_crud.model.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;

@Slf4j
@Repository
public class UserDaoHibernateImpl implements UserDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public User getUser(long id) {
        return entityManager.find(User.class, id);
    }

    @Override
    public String getUserPassword(long id) {
        try {
            TypedQuery<String> query = entityManager.createQuery("select password from User where id = :id", String.class);
            query.setParameter("id", id);
            return query.getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public User getUserByEmail(String email) {
        TypedQuery<User> query = entityManager.createQuery("from User where email = :email", User.class);
        query.setParameter("email", email);
        return query.getSingleResult();

    }


    @Override
    public List<User> getAllUsers() {
        TypedQuery<User> query = entityManager.createQuery("from User", User.class);
        return query.getResultList();
    }


    @Override
    public void deleteUser(long id) {
        Query query = entityManager.createQuery("delete User where id = :id");
        query.setParameter("id", id);
        query.executeUpdate();
        log.debug("Deleted: " + id);
    }


    @Override
    public void deleteAll() {
        entityManager.createQuery("delete from User").executeUpdate();
        log.debug("Deleted all users");
    }


    @Override
    public void addUser(User user) {
        entityManager.persist(user);
    }


    @Override
    public void updateUser(User newUser) {
        entityManager.merge(newUser);
    }


}
