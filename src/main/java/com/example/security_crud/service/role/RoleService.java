package com.example.security_crud.service.role;


import com.example.security_crud.model.Role;

import java.util.List;

public interface RoleService {
    List<Role> getAllRoles();

    List<Role> getRolesByIds(List<Long> ids);

    void create(Role role);

    void delete(long id);

    void update(Role role);
}
