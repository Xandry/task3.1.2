package com.example.security_crud.service.role;

import com.example.security_crud.dao.role.RoleDao;
import com.example.security_crud.model.Role;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {

    private final RoleDao roleDao;

    public RoleServiceImpl(RoleDao roleDao) {
        this.roleDao = roleDao;
    }

    @Override
    public List<Role> getAllRoles() {
        return roleDao.getAllRoles();
    }

    @Override
    public List<Role> getRolesByIds(List<Long> ids) {
        return roleDao.getRolesByIds(ids);
    }

    @Override
    public void create(Role role) {
        roleDao.add(role);
    }

    @Override
    public void delete(long id) {
        roleDao.delete(id);
    }

    @Override
    public void update(Role role) {
        roleDao.update(role);
    }
}
