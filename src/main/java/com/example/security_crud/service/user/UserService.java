package com.example.security_crud.service.user;

import com.example.security_crud.model.User;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserService extends UserDetailsService {

    User getUser(long id);

    User getByEmail(String email);

    String getPasswordByUserId(long id);

    List<User> getAllUsers();

    void deleteUser(long id);

    void deleteAll();

    void addUser(User user);

    void updateUser(User user);

}
